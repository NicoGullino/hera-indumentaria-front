import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_RADIO_DEFAULT_OPTIONS } from '@angular/material/radio';
import { ActivatedRoute, Router } from '@angular/router';
import { PrendaService } from 'src/app/services/prenda.service';
import { Prenda } from '../../models/prenda';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-agregar-editar-prenda',
  templateUrl: './agregar-editar-prenda.component.html',
  styleUrls: ['./agregar-editar-prenda.component.css'],
  providers: [{
    provide: MAT_RADIO_DEFAULT_OPTIONS,
    useValue: { color: 'primary' },
}]
})
export class AgregarEditarPrendaComponent implements OnInit {
  pagos : any[]= ['Pago', 'No Pago'];
  idPrenda: any;
  accion = 'Crear';

  myForm: FormGroup;

  constructor(private fb: FormBuilder,
              private prendaService: PrendaService,
              private route: Router ,
              private snackBar: MatSnackBar,
              private aRoute: ActivatedRoute) {
              this.myForm = this.fb.group({
                descripcion: ['', [Validators.required]],
                precioCompra: ['', [Validators.required]],
                precioVenta: ['', [Validators.required]],
                diferenciaPrecio: ['', [Validators.required]],
                pago: ['', [Validators.required]],
                pagoParcial: ['', [Validators.required]],
                nombreComprador: ['', [Validators.required]],
                nombreVendedor: ['', [Validators.required]],
              }); 
              const idParam = 'id';
              this.idPrenda = this.aRoute.snapshot.params[idParam];
  }


  ngOnInit(): void {
  if(this.idPrenda !== undefined){
      this.accion = 'Editar';
      this.esEditar();
    }
  }

  guardarPrenda(){
    console.log(this.myForm);
    const prenda: Prenda = {
      id: this.myForm.get('id')?.value,
      descripcion: this.myForm.get('descripcion')?.value,
      precioCompra: this.myForm.get('precioCompra')?.value,
      precioVenta: this.myForm.get('precioVenta')?.value,
      diferenciaPrecio: this.myForm.get('diferenciaPrecio')?.value,
      pago: this.myForm.get('pago')?.value,
      pagoParcial: this.myForm.get('pagoParcial')?.value,
      nombreComprador: this.myForm.get('nombreComprador')?.value,
      nombreVendedor: this.myForm.get('nombreVendedor')?.value
    };
    if(this.idPrenda !== undefined){
      this.editarPrenda(prenda);
    }else {
      this.agregarPrenda(prenda);
    }
  }
  agregarPrenda(prenda: Prenda){
    this.prendaService.agregarPrenda(prenda);
    this.snackBar.open('La prenda fue registrada con éxito!','', {
          duration: 3000
        });
    this.route.navigate(['/']);
  }
  editarPrenda(prenda: Prenda){
    this.prendaService.editPrenda(prenda, this.idPrenda);
    this.snackBar.open('La prenda fue actualizada con éxito!','', {
      duration: 3000
    });
    this.route.navigate(['/']);
  }
  esEditar(){
    const prenda: Prenda = this.prendaService.getPrenda(this.idPrenda);
    console.log(prenda);
    this.myForm.patchValue({
      descripcion: prenda.descripcion,
      precioCompra: prenda.precioCompra,
      precioVenta: prenda.precioVenta,
      diferenciaPrecio: prenda.diferenciaPrecio,
      pago: prenda.pago,
      pagoParcial: prenda.pagoParcial,
      nombreComprador: prenda.nombreComprador,
      nombreVendedor: prenda.nombreVendedor
    })
  }

}
