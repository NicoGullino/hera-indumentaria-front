import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarEditarPrendaComponent } from './agregar-editar-prenda.component';

describe('AgregarEditarPrendaComponent', () => {
  let component: AgregarEditarPrendaComponent;
  let fixture: ComponentFixture<AgregarEditarPrendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarEditarPrendaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarEditarPrendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
