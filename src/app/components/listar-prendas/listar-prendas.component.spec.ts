import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarPrendasComponent } from './listar-prendas.component';

describe('ListarPrendasComponent', () => {
  let component: ListarPrendasComponent;
  let fixture: ComponentFixture<ListarPrendasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarPrendasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarPrendasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
