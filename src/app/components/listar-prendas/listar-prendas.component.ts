import { Component, OnInit, ViewChild } from '@angular/core';
import { Prenda } from '../../models/prenda';
import { PrendaService } from '../../services/prenda.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { MensajeConfirmacionComponent } from '../shared/mensaje-confirmacion/mensaje-confirmacion.component';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-listar-prendas',
  templateUrl: './listar-prendas.component.html',
  styleUrls: ['./listar-prendas.component.css']
})
export class ListarPrendasComponent implements OnInit {

  public listPrenda: Prenda[];
  public result : string;
  @ViewChild(MatSort, {static: true})sort: MatSort;
  constructor(private prendaService: PrendaService,
              public dialog: MatDialog){
              this.listPrenda = [],
              this.sort = new MatSort, 
              this.result =''} 

  ngOnInit() {
    this.cargarPrendas();
  }
  public cargarPrendas(){ 
    this.prendaService.getPrendas().subscribe(
      (response: Prenda[]) => {
        this.listPrenda = response;
        console.log(this.listPrenda);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  eliminarEmpleado(index: number){
    const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
      width: '350px',
      data: {mensaje: '¿Está seguro que desea eliminar el empleado?'}
    });
    
    //dialogRef.afterClosed().subscribe(result => {
    //  if(result === 'aceptar'){
    //    this.prendaService.eliminarPrenda(index);
    //    this.cargarPrendas();
     //   this.snackBar.open('El empleado fue eliminado con éxito!','', {
      //    duration: 3000
       // });
     //}
    //});
    }

}
