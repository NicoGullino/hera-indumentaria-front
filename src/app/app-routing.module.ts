import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarPrendasComponent } from './components/listar-prendas/listar-prendas.component';
import { AgregarEditarPrendaComponent } from './components/agregar-editar-prenda/agregar-editar-prenda.component';

const routes: Routes = [
  { path: 'agregar', component: AgregarEditarPrendaComponent },
  { path: '', component: ListarPrendasComponent },
  { path: 'editar/:id', component: AgregarEditarPrendaComponent },
  //los : indican que es dinámico
  { path: '**', component: ListarPrendasComponent }
  //los ** indican que si el usuario intenta una ruta no contemplada, 
  //lo redirija a ListarPrendasComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
