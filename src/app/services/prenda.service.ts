import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Prenda } from '../models/prenda';


@Injectable({
  providedIn: 'root'
})
export class PrendaService {
  listPrenda: Prenda[];

  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient){
    this.listPrenda = [];
  }
  getPrenda(index: number){
    return this.listPrenda[index];
  }
  public getPrendas(): Observable<Prenda[]> {
    return this.http.get<Prenda[]>(`${this.apiServerUrl}/prenda/all`);
  }
  eliminarPrenda(index: number){
    this.listPrenda.splice(index, 1);
  }
  getEmpleado(index: number){
    return this.listPrenda[index];
  }
  agregarPrenda(prenda : Prenda){ 
    this.listPrenda.unshift(prenda);
  }
  public editPrenda(prenda: Prenda, idPrenda: number){
    this.listPrenda[idPrenda].descripcion = prenda.descripcion;
    this.listPrenda[idPrenda].precioCompra = prenda.precioCompra;
    this.listPrenda[idPrenda].precioVenta = prenda.precioVenta;
    this.listPrenda[idPrenda].diferenciaPrecio = prenda.diferenciaPrecio;
    this.listPrenda[idPrenda].pago = prenda.pago;
    this.listPrenda[idPrenda].pagoParcial = prenda.pagoParcial;
    this.listPrenda[idPrenda].nombreComprador = prenda.nombreComprador;
    this.listPrenda[idPrenda].nombreVendedor = prenda.nombreVendedor;
  }
}
