export interface Prenda {
    id: number;
    descripcion: string;
    precioCompra: BigInteger;
    precioVenta: BigInteger;
    diferenciaPrecio: BigInteger;
    pago: boolean;
    pagoParcial: BigInteger;
    nombreComprador: string;
    nombreVendedor: string;
}
  